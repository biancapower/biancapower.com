---
layout: page
title: About
# featured_image: /assets/images/pages/about.jpg
---


I'm an enthusiastic Ruby on Rails developer and an educator at heart. I have excellent communication skills and am a natural at breaking down concepts and helping others to learn. I care about the projects I work on and the people I work on them for and with – the end user as much as my colleagues and key stakeholders. When it comes to getting things done I’m always happy to roll up my sleeves and work with my team to find a way. I'm an easy-going, down to earth person with a passion for helping others get from where they are now to where they want to be.

Learn more about my skills and experience [here]({{ site.url }}/hire-me/){:target="_blank"}.
