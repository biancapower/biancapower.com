---
layout: page
title: Projects
# featured_image: /assets/images/pages/about.jpg
---

<div class="project-grid">

{% for project in site.data.projects.projects %}
{% include project.html %}
{% endfor %}

</div>
