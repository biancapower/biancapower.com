---
layout: page
title: My Skills & Experience
# featured_image: /assets/images/pages/about.jpg
---


<!-- {% youtube "https://www.youtube.com/watch?v=R9u2e66IKzc" %}

--- -->

Over the years I've amassed a range of experiences across education, leadership, and tech. Highlights include a **PhD in education**, learning to **code**, and **leading a team** of educators and subject matter experts in the development and delivery of Higher Education accredited **web development bootcamps**.

### Day Job

In my day job at [Waivpay](https://www.waivpay.com/){:target="_blank"} I work across several applications, all of which are Rails & React apps. The technology includes:
- Ruby on Rails (v5 & v6)
	- PostgreSQL
	- ActiveAdmin
	- Sidekiq
	- AWS S3
	- Devise
	- CanCanCan & Pundit (across different apps)
	- RSpec
	- Stripe
	- Eway
	- Twilio
- React.js
	- Jest
	- Redux
- Heroku

I've also contributed to the [current WIP company website](https://develop--waivpay.netlify.app/), which uses:
- Gatsby.js
- GraphQL
- Contentful CMS
- Netlify
- Google Maps
	- for [this integration](https://develop--waivpay.netlify.app/locations) that I built to facilitate searching for closest locations

### Side Projects

While more can be found on my [GitHub](https://github.com/biancapower/){:target="_blank"} and [GitLab](https://gitlab.com/biancapower/){:target="_blank"}, and a nicer overview on the [Projects]({{ site.url }}/projects/){:target="_blank"} page, here are a few links to side projects I've worked on:
- [Cameras R Us](https://cameras-r-us.fly.dev/){:target="_blank"}
	- Rails 6 app using Stimulus JS, QR Code functionality and Google Maps Places API
	- Repo [here](https://github.com/biancapower/cameras-r-us){:target="_blank"}
- [Generic Marketplace](https://generic-marketplace.fly.dev/){:target="_blank"}
	- 2 sided marketplace build on Rails 6
	- Repo [here](https://github.com/biancapower/generic-marketplace){:target="_blank"}
- [Matters](https://matters.fly.dev/){:target="_blank"}
	- Minimalist note-taking app built on Rails 7, using Hotwire
	- Started in Jan 2023 - still an early work in progress
	- Repo [here](https://gitlab.com/biancapower/matters){:target="_blank"}
- [Turbo Tasks](https://turbo-tasks.fly.dev/){:target="_blank"}
	- To-do list app built on Rails 7, using Hotwire
	- Started in Jan 2023 - still an early work in progress
	- Repo [here](https://gitlab.com/biancapower/tasks){:target="_blank"}
- [biancapower.com](https://biancapower.com/){:target="_blank"}
	- This website! A portfolio site built on Jekyll
	- Repo [here](https://gitlab.com/biancapower/biancapower.com){:target="_blank"}


> More details can be found by clicking on the headings below, and my CV can be downloaded [here]({{ site.url }}/assets/bianca_power_cv.pdf){:target="_blank"}.

{% include accordion.html
		title="Coding"
		description="
		In my spare time I enjoy doing programming challenges in Ruby, building things with Rails (loving Rails 7 and Hotwire right now!), and generally learning new things.
		<br/>
		<br/>
		In my day job I work with:
		<ul>
			<li>Ruby on Rails (v5 & v6)</li>
			<li>React.js</li>
			<li>Redux</li>
			<li>Gatsby.js</li>
		</ul>

		I also have some familiarity with:
		<ul>
			<li>Python</li>
			<li>Bash scripting</li>
			<li>Jekyll</li>
			<li>GraphQL</li>
		</ul>

		I love using dev tools and methodologies such as:
		<ul>
			<li>Unix Terminal</li>
			<li>git</li>
			<li>GitHub & GitLab</li>
			<li>vim</li>
			<li>VS Code</li>
			<li>Docs as code</li>
		</ul>"
%}
{% include accordion.html
		title="Leadership"
		description="
		<ul>
			<li>Managing a team of 10-20 neurodiverse Coding Bootcamp educators</li>
			<li>Currently studying MBA (Leadership)</li>
			<li>Prolific reader of HBR</li>
		</ul>"
%}
{% include accordion.html
		title="Education & Training"
		description="
		<ul>
			<li>Over 10yrs of teaching before, during, and post-pandemic
				<ul>
					<li>Face to Face classroom</li>
					<li>Virtual</li>
					<li>Hybrid</li>
				</ul>
			</li>
			<li>Teaching a variety of content
				<ul>
					<li>Educational theory and practice</li>
					<li>Research methods (quantitative & qualitative)</li>
					<li>Coding fundamentals (Ruby / Rails / Python / JavaScript / C / HTML & CSS)</li>
					<li>Developer tools and methodology (git, unix terminal, GitHub, Agile)</li>
				</ul>
			</li>
		</ul>"
%}
{% include accordion.html
		title="Research"
		description="
		<ul>
			<li>PhD thesis in Education</li>
			<li>5yrs of experience as a Research Assistant (quant / qual)</li>
			<li>Co-author of numerous academic papers</li>
			<li>Multiple conference presentations</li>
		</ul>"
%}

### Examples of my Communication Style

While most of my work is company product and 'behind closed doors', I can share a few bits and pieces:
- Blog posts
	- On [my website](https://biancapower.com/), which is [hosted on GitLab Pages](https://gitlab.com/biancapower/biancapower.com){:target="_blank"} using Jekyll
		- Writing a Bash function to automate Exercism.io JS exercise setup
		- How to Combine Git Commits - Squash 'em with a rebase!
		- How to add ASCII art around code comment blocks in vim
		- How to change your default text editor for git (and avoid vim)
	- On [Medium](https://medium.com/@biancapower){:target="_blank"}
		- How to add Bootstrap 4 to a Rails 5 app
		- Why your Rails site with Bootstrap might not display correctly on mobile devices
		- Rails 5 `link_to` trick

- Verbal communication style
	- Interviews
		- [Q&A about Coder Academy’s current Bootcamps](https://bit.ly/CA_QnA){:target="_blank"} (starting at 27 mins)
		- [Is a Coding Bootcamp worth it?](https://bit.ly/BP_JD){:target="_blank"}
	- Promo videos
		- 20 seconds on [Breaking the Bias of who can work in tech](https://bit.ly/BreakTheBiasIWD){:target="_blank"}
		- [Not All Hackers Wear Hoodies](https://youtu.be/GDjDzI1s554){:target="_blank"}
		- [Cyber Security Bootcamp](https://youtu.be/liUs8s_shvk){:target="_blank"}

- Academic Publications
	- Thesis
		- PhD Thesis (2014): [Portraits of Quality Arts Education in Australian Primary
School Classrooms](https://research-repository.griffith.edu.au/bitstream/handle/10072/366233/Power_2014_02Thesis.pdf){:target="_blank"}
	- Peer-reviewed Papers
		- Links on [Google Scholar](https://scholar.google.com.au/citations?user=kNDwtXQAAAAJ&hl=en){:target="_blank"}
